### TUTORIALS ##
# Django+pg: https://www.digitalocean.com/community/tutorials/how-to-set-up-django-with-postgres-nginx-and-gunicorn-on-ubuntu-16-04#install-the-packages-from-the-ubuntu-repositories
# Cassandra: http://www.itzgeek.com/how-tos/linux/ubuntu-how-tos/install-cassandra-on-ubuntu-16-04-and-run-a-single-node-cassandra-cluster-on-ubuntu.html
# Redis: https://www.digitalocean.com/community/tutorials/how-to-install-and-configure-redis-on-ubuntu-16-04
#
### From: https://stackoverflow.com/questions/42662104/how-to-install-pip-for-python-3-6-on-ubuntu-16-10/44254088#44254088
apt install software-properties-common
add-apt-repository ppa:jonathonf/python-3.6
apt update
### PYTHON 3.6
# To avoid 'x86_64-linux-gnu-gcc' failed with exit status 1:
apt install build-essential libssl-dev libffi-dev python-dev -y

apt install python3.6 python3.6-dev python3.6-venv python3-pip -y
wget https://bootstrap.pypa.io/get-pip.py
python3.6 get-pip.py
rm get-pip.py
# Make python3.6 the default python
ln -s /usr/bin/python3.6 /usr/local/bin/python3
ln -s /usr/local/bin/pip /usr/local/bin/pip3
# Do this only if you want python3 to be the default Python
# instead of python2 (may be dangerous, esp. before 2020):
ln -s /usr/bin/python3.6 /usr/local/bin/python
#### or this to change the default version
#update-alternatives --install /usr/bin/python3 python3 /usr/bin/python3.5 1
#update-alternatives --install /usr/bin/python3 python3 /usr/bin/python3.6 2
#update-alternatives --config python3
#####
pip3 install virtualenv
pip3 install --upgrade pip

### CASSANDRA
add-apt-repository ppa:webupd8team/java
apt-get update
apt-get -y install oracle-java8-installer
echo "deb http://www.apache.org/dist/cassandra/debian 36x main" | sudo tee -a /etc/apt/sources.list.d/cassandra.list
gpg --keyserver pgp.mit.edu --recv-keys 749D6EEC0353B12C
gpg --export --armor 749D6EEC0353B12C | sudo apt-key add -
apt-get update
apt-get install cassandra -y

### REDIS
username="glifadmin"
useradd $username
echo "glifadmin ALL=(ALL:ALL) ALL" >> /etc/sudoers
apt-get install build-essential tcl
cd /tmp
sudo -u $username curl -O http://download.redis.io/redis-stable.tar.gz
sudo -u $username tar xzvf redis-stable.tar.gz
cd redis-stable
sudo -u $username make
sudo -u $username make test
make install
make test

# Configure redis:
# Change "supervised no" to "supervised systemd"
sed -i 's/^\(supervised\s*\).*$/\1systemd/' redis.conf
# Change "dir ./" to "/var/lib/redis"
sed -i 's/^\(dir\s*\).*$/\1\/var\/lib\/redis/' redis.conf
mkdir /etc/redis
touch /etc/redis/redis.conf
cp /tmp/redis-stable/redis.conf /etc/redis/redis.conf
touch /etc/systemd/system/redis.service
# Redis service config:
echo "[Unit]" >> /etc/systemd/system/redis.service
echo "Description=Redis In-Memory Data Store" >> /etc/systemd/system/redis.service
echo "After=network.target" >> /etc/systemd/system/redis.service
echo "[Service]" >> /etc/systemd/system/redis.service
echo "User=redis" >> /etc/systemd/system/redis.service
echo "Group=redis" >> /etc/systemd/system/redis.service
echo "ExecStart=/usr/local/bin/redis-server /etc/redis/redis.conf" >> /etc/systemd/system/redis.service
echo "ExecStop=/usr/local/bin/redis-cli shutdown" >> /etc/systemd/system/redis.service
echo "Restart=always" >> /etc/systemd/system/redis.service
echo "[Install]" >> /etc/systemd/system/redis.service
echo "WantedBy=multi-user.target" >> /etc/systemd/system/redis.service

adduser --system --group --no-create-home redis
mkdir /var/lib/redis
chown redis:redis /var/lib/redis
chmod 770 /var/lib/redis
# Enable Redis to Start at Boot
systemctl enable redis
systemctl start redis

### RABBITMQ
# Erlang
touch /etc/apt/preferences.d/erlang
# Add into file:
echo "Package: erlang*" >> /etc/apt/preferences.d/erlang
echo "Pin: version 1:19.3-1" >> /etc/apt/preferences.d/erlang
echo "Pin-Priority: 1000" >> /etc/apt/preferences.d/erlang
echo "Package: esl-erlang" >> /etc/apt/preferences.d/erlang
echo "Pin: version 1:19.3.6" >> /etc/apt/preferences.d/erlang
echo "Pin-Priority: 1000" >> /etc/apt/preferences.d/erlang

#!! Add host's name to /etc/hosts inline with localhost
apt install erlang -y
echo 'deb http://www.rabbitmq.com/debian/ testing main' |
     sudo tee /etc/apt/sources.list.d/rabbitmq.list
wget -O- https://www.rabbitmq.com/rabbitmq-release-signing-key.asc |
     sudo apt-key add -
apt-get update
apt-get install rabbitmq-server -y


### POSTGRESQL
apt install postgresql postgresql-contrib -y
sudo -u postgres psql -c "CREATE DATABASE glyphstock"
sudo -u postgres psql -c "CREATE USER glyphadmin WITH PASSWORD 'glyphadmin'"
sudo -u postgres psql -c "ALTER ROLE glyphadmin SET client_encoding TO 'utf8'"
sudo -u postgres psql -c "ALTER ROLE glyphadmin SET default_transaction_isolation TO 'read committed'"
sudo -u postgres psql -c "ALTER ROLE glyphadmin SET timezone TO 'UTC';"
# Add admin rights to user "glyphadmin" (must be done before
#configuring external acces, otherwise a UNIX socket error arises):
sudo -u postgres psql -c "GRANT ALL PRIVILEGES ON DATABASE glyphstock TO glyphadmin;"

# Change "listen_adresses = '*'":
# /etc/postgresql/9.5/main/postgresql.conf
# Allow access (append 'host  all  all 0.0.0.0/0 md5'):
echo "host  all  all 0.0.0.0/0 md5" >> /etc/postgresql/9.5/main/pg_hba.conf
/etc/init.d/postgresql restart

# PgAdmin (INSTALL IT AT USER MACHINE, AND THEN CONNECT TO THE DESIRED DB SERVER)
#virtualenv pgAdminENV
#source pgAdminENV/bin/activate
#wget https://ftp.postgresql.org/pub/pgadmin/pgadmin4/v1.5/pip/pgadmin4-1.5-py2.py3-none-any.whl
#pip install ./pgadmin4-1.3-py2.py3-none-any.whl
# pgAdmin default config works on localhost, there're two options:
# 1) Change config at:
#       pgAdminENV/lib/python3.6/site-packages/pgadmin4/config.py
#    Then:
#       python pgAdminENV/lib/python3.6/site-packages/pgadmin4/pgAdmin4.py
# 2) Run it as a WSGI application behind Apache or Nginx


### DJANGO
cd
git clone http://gitlab.intra.glyphstock.com/ballbrk/goodrobot_server
mv goodrobot_server/ goodrobot_api/
mkdir goodrobot_api/apps/goodrobot_payments

git clone http://gitlab.intra.glyphstock.com/ballbrk/goodrobot_payments
ln -s ~/goodrobot_payments/ ~/goodrobot_api/apps/goodrobot_payments

cd goodrobot_api
mkdir -p public/assets

virtualenv venv
source venv/bin/activate

pip install django psycopg2
pip install -r requirements.txt
pip install -r requirements/base.txt
pip install -r requirements/local.txt
pip install django-cors-headers
apt install optipng -y
apt install jpegoptim -y
pip install django-extra-fields

./manage.py makemigrations --settings goodrobot_api.settings.developer.devel
./manage.py migrate --settings goodrobot_api.settings.developer.devel
./manage.py createsuperuser --email systems@glyphstock.com --user_type admin
nohup ./manage.py runserver 0.0.0.0:8000 --settings goodrobot_api.settings.developer.devel
