#Hay que guardar en la variable 'ip' la direccion de la máquina que va a contener el servicio omd
#Si la máquina esta en la red interna de la empresa, basta con la ip de la máquina
#Si la máquina esta en un red exterior, hay que poner la ip/ips pública de la empresa
ip="10.100.10.180"
#Instalación cliente
apt install check-mk-agent
apt install xinetd -y #puede que ya este instalado
#habilitar puerto 6556
#editar /etc/xinetd.d/check_mk
#descomentar line #from_only=... y poner la ip de la maquina que contiene el servidor OMD
sed -i "s/\t#only_from/\tonly_from/" /etc/xinetd.d/check_mk #sirve
sed -i "s/127.0.0.1 10.0.20.1 10.0.20.2/$ip/" /etc/xinetd.d/check_mk

#si xinetd no estaba instalado
service xinetd start
service xinetd enable

#si lo estaba
service xinetd restart