apt-get install ntp -y

### PYTHON 3.6
### From: https://stackoverflow.com/questions/42662104/how-to-install-pip-for-python-3-6-on-ubuntu-16-10/44254088#44254088
apt install software-properties-common
add-apt-repository ppa:jonathonf/python-3.6
apt update

# To avoid 'x86_64-linux-gnu-gcc' failed with exit status 1:
apt install build-essential libssl-dev libffi-dev python-dev -y

apt install python3.6 python3.6-dev python3.6-venv python3-pip -y
wget https://bootstrap.pypa.io/get-pip.py
python3.6 get-pip.py
rm get-pip.py
# Make python3.6 the default python
ln -s /usr/bin/python3.6 /usr/local/bin/python3
ln -s /usr/local/bin/pip /usr/local/bin/pip3
# Do this only if you want python3 to be the default Python
# instead of python2 (may be dangerous, esp. before 2020):
ln -s /usr/bin/python3.6 /usr/local/bin/python
#### or this to change the default version
#update-alternatives --install /usr/bin/python3 python3 /usr/bin/python3.5 1
#update-alternatives --install /usr/bin/python3 python3 /usr/bin/python3.6 2
#update-alternatives --config python3
#####
pip3 install virtualenv
pip3 install --upgrade pip


### NGINX
apt-get install nginx
cp /etc/nginx/sites-available/default /etc/nginx/sites-available/glyphstock.com

# Remove default_server from '/etc/nginx/sites-available/glyphstock.com'
# Configure 'glyphstock.com' server block (virtualhost)
# Enable the server block 'glyphstock.com'
ln -s /etc/nginx/sites-available/glyphstock.com /etc/nginx/sites-enabled/
# In order to avoid a possible hash bucket memory problem that can arise from
# adding additional server names, we will go ahead and adjust a single value
# within our /etc/nginx/nginx.conf file.
# Within the file, find the server_names_hash_bucket_size directive. Remove
# the '#' symbol to uncomment the line

# Test Nginx config
nginx -t

systemctl restart nginx

# SSL using 'Let's Encrypt' (NO, as it is gonna be proxied)
#add-apt-repository ppa:certbot/certbot
#apt-get update
#apt-get install python-certbot-nginx
#certbot --nginx
## 'certbot --nginx certonly' to make change to Nginx by hand



### RABBITMQ
# Erlang
touch /etc/apt/preferences.d/erlang
# Add into file:
echo "Package: erlang*" >> /etc/apt/preferences.d/erlang
echo "Pin: version 1:19.3-1" >> /etc/apt/preferences.d/erlang
echo "Pin-Priority: 1000" >> /etc/apt/preferences.d/erlang
echo "Package: esl-erlang" >> /etc/apt/preferences.d/erlang
echo "Pin: version 1:19.3.6" >> /etc/apt/preferences.d/erlang
echo "Pin-Priority: 1000" >> /etc/apt/preferences.d/erlang

#!! Add host's name to /etc/hosts inline with localhost
apt install erlang -y
echo 'deb http://www.rabbitmq.com/debian/ testing main' |
     sudo tee /etc/apt/sources.list.d/rabbitmq.list
wget -O- https://www.rabbitmq.com/rabbitmq-release-signing-key.asc |
     sudo apt-key add -
apt-get update
apt-get install rabbitmq-server -y
