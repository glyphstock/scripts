apt install haproxy -y

# TEST config
# haproxy -c /etc/haproxy/haproxy.cfg

# With 'Letsencypt'
mkdir -p /etc/haproxy/certs
DOMAIN='api.glyphstock.com' sudo -E bash -c 'cat /etc/letsencrypt/live/$DOMAIN/fullchain.pem /etc/letsencrypt/live/$DOMAIN/privkey.pem > /etc/haproxy/certs/$DOMAIN.pem'
chmod -R go-rwx /etc/haproxy/certs
