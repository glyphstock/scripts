umount -a
echo "tmpfs /tmp tmpfs rw,noexec,nosuid,async,noatime,size=5G,nodev 0 0" >> /etc/fstab
mount

echo "94.130.53.169   ha1
94.130.54.106   ha2
94.130.53.31    pg1
94.130.53.30    pg2
94.130.35.135   cas1
88.99.145.84    cas2" >> /etc/hosts
apt update && apt install ntp -y


### POSTGRES CLIENT 9.6
sh -c 'echo "deb http://apt.postgresql.org/pub/repos/apt/ xenial-pgdg main" > /etc/apt/sources.list.d/pgdg.list'
apt-get install wget ca-certificates -y
wget --quiet -O - https://www.postgresql.org/media/keys/ACCC4CF8.asc | apt-key add -
apt update && apt upgrade -y
apt install postgresql-client-9.6 -y


### PYTHON 3.6
### From: https://stackoverflow.com/questions/42662104/how-to-install-pip-for-python-3-6-on-ubuntu-16-10/44254088#44254088
apt install software-properties-common -y
add-apt-repository ppa:jonathonf/python-3.6 -y
apt update

apt install python3.6 python3.6-dev python3.6-venv python3-pip -y
wget https://bootstrap.pypa.io/get-pip.py
python3.6 get-pip.py
rm get-pip.py
# Make python3.6 the default python
ln -s /usr/bin/python3.6 /usr/local/bin/python3
ln -s /usr/local/bin/pip /usr/local/bin/pip3
# Do this only if you want python3 to be the default Python
# instead of python2 (may be dangerous, esp. before 2020):
ln -s /usr/bin/python3.6 /usr/local/bin/python
#### or this to change the default version
#update-alternatives --install /usr/bin/python3 python3 /usr/bin/python3.5 1
#update-alternatives --install /usr/bin/python3 python3 /usr/bin/python3.6 2
#update-alternatives --config python3
#####
pip3 install virtualenv
pip3 install --upgrade pip


### NGINX
apt install nginx -y
cp /etc/nginx/sites-available/default /etc/nginx/sites-available/glyphstock.com

# Remove default_server from '/etc/nginx/sites-available/glyphstock.com'
sed -i.backup -e "s/listen 80 default_server;/listen 80;/" /etc/nginx/sites-available/glyphstock.com
sed -i.backup -e "s/listen \[\:\:\]\:80 default_server;/listen \[\:\:\]\:80;/" /etc/nginx/sites-available/glyphstock.com
# Configure 'glyphstock.com' server block (virtualhost)
# Enable the server block 'glyphstock.com'
ln -s /etc/nginx/sites-available/glyphstock.com /etc/nginx/sites-enabled/
# In order to avoid a possible hash bucket memory problem that can arise from
# adding additional server names, we will go ahead and adjust a single value
# within our /etc/nginx/nginx.conf file.
# Within the file, find the server_names_hash_bucket_size directive. Remove
# the '#' symbol to uncomment the line

# Test Nginx config
nginx -t

systemctl restart nginx

# SSL using 'Let's Encrypt' (NO, as it is gonna be proxied)
#add-apt-repository ppa:certbot/certbot
#apt-get update
#apt-get install python-certbot-nginx
#certbot --nginx
## 'certbot --nginx certonly' to make change to Nginx by hand


### RABBITMQ
# Erlang
touch /etc/apt/preferences.d/erlang
# Add into file:
echo "Package: erlang*" >> /etc/apt/preferences.d/erlang
echo "Pin: version 1:19.3-1" >> /etc/apt/preferences.d/erlang
echo "Pin-Priority: 1000" >> /etc/apt/preferences.d/erlang
echo "Package: esl-erlang" >> /etc/apt/preferences.d/erlang
echo "Pin: version 1:19.3.6" >> /etc/apt/preferences.d/erlang
echo "Pin-Priority: 1000" >> /etc/apt/preferences.d/erlang

#!! Add host's name to /etc/hosts inline with localhost
apt install erlang -y
echo 'deb http://www.rabbitmq.com/debian/ testing main' |
     sudo tee /etc/apt/sources.list.d/rabbitmq.list
wget -O- https://www.rabbitmq.com/rabbitmq-release-signing-key.asc |
     sudo apt-key add -
apt update
apt install rabbitmq-server -y

#Open ports 25, 443 and 80
#Falta ufw enable
apt install ufw -y
sudo ufw allow 25/tcp
sudo ufw allow 443/tcp
sudo ufw allow 80/tcp
sudo ufw allow 22222/tcp #futuras conexiones ssh
sudo ufw allow 6556/tcp #Conexion omd

apt install optipng -y
apt install jpegoptim -y

#Supervisor
apt install supervisor -y

#añadir usuario para las apis (pwd: x-54173*2e7gnms1779p!)
adduser --disabled-login --gecos "" glyphuser
echo -e 'x-54173*2e7gnms1779p!\nx-54173*2e7gnms1779p!' | passwd glyphuser

#####################################################
### INSTALL pgPool (at API servers):
#####################################################
echo "deb http://apt.postgresql.org/pub/repos/apt/ xenial-pgdg main" > /etc/apt/sources.list.d/pgdg.list
apt-get install wget ca-certificates -y
wget --quiet -O - https://www.postgresql.org/media/keys/ACCC4CF8.asc | apt-key add -
apt-get update && apt-get upgrade -y
apt install postgresql-9.6 -y
apt-get install postgresql-server-dev-all -y
apt-get install postgresql-common -y

apt-get install libpq-dev make -y --allow-unauthenticated
#apt install libpq-dev -y
apt-get install build-essential -y
wget http://www.pgpool.net/download.php?f=pgpool-II-3.6.6.tar.gz -O pgpool-II-3.6.6.tar.gz
tar -xzf pgpool-II-3.6.6.tar.gz
rm pgpool-II-3.6.6.tar.gz
cd pgpool-II-3.6.6

# INSTALL PGPOOL (under /usr/local)
./configure 
#./configure --prefix=/usr/share/pgpool2/3.6.6
make
make install

# PGPOOL-RECOVERY
cd pgpool-II-3.6.6/src/sql/pgpool-recovery
make
make install

sudo -u postgres psql template1 -c  "CREATE EXTENSION pgpool_recovery;"

echo "pgpool.pg_ctl = '/usr/lib/postgresql/9.6/bin/pg_ctl'" >> /etc/postgresql/9.6/main/postgresql.conf

sudo -i -u postgres /usr/lib/postgresql/9.6/bin/pg_ctl -D /var/lib/postgresql/9.6/main restart
echo

# PGPOOL-REGCLASS (Skipped as we are using a later version than PostgreSQL 9.4)
# cd ~/pgpool-II-3.6.6/src/sql/pgpool-recovery
#make
#make install
#sudo -u postgres psql template1
#CREATE EXTENSION pgpool_regclass;
#\q

# INSERT_LOCK TABLE (skipped as we are noy using replication)
#cd pgpool-II-3.6.6/sql
#psql -f insert_lock.sql template1

# CREATE PGPOOLII service
# /etc/default/pgpool2
# CREATE PGPOOLII init.d
# /etc/default/pgpool2
# BOTH VIA SCP
# scp -r etc/default/ api2:/etc/
#scp -r etc/init.d api2:/etc/


# Setting up pgpool.conf (master-slave streaming replication)
cp /usr/local/etc/pgpool.conf.sample-stream /usr/local/etc/pgpool.conf

# PGPOOL user
useradd pgpool
echo -e 'gnms1779p\ngnms1779p' | passwd pgpool

chown pgpool /usr/local/etc/pgpool.conf

# General settings
#-e "s/^listen_addresses = 'localhost'/listen_addresses = '*'/" \
sed -i.backup \
-e "s+^socket_dir = '/tmp'+socket_dir = '/var/run/postgresql'+" \
-e "s+^pcp_socket_dir = '/tmp'+pcp_socket_dir = '/var/run/postgresql'+" \
/usr/local/etc/pgpool.conf

# Backend settings
sed -i.backup \
-e "s/^backend_hostname0 = 'host1'/backend_hostname0 = 'pg1'/" \
-e "s/^backend_port0 = 5432/backend_port0 = 5433/" \
-e "s+^backend_data_directory0 = '/data'+backend_data_directory0 = '/var/lib/postgresql/9.6/main'+" \
/usr/local/etc/pgpool.conf

sed -i.backup \
-e "s/^#backend_hostname1 = 'host2'/backend_hostname1 = 'pg2'/" \
-e "s/^#backend_port1 = 5433/backend_port1 = 5433/" \
-e "s/^#backend_weight1 = 1/backend_weight1 = 1/" \
-e "s+^#backend_data_directory1 = '/data1'+backend_data_directory1 = '/var/lib/postgresql/9.6/main'+" \
-e "s/^#backend_flag1 = 'ALLOW_TO_FAILOVER'/backend_flag1 = 'ALLOW_TO_FAILOVER'/" \
/usr/local/etc/pgpool.conf

# Pooling settings
# Logging

# Health check
sed -i.backup \
-e "s/^health_check_period = 0/health_check_period = 5/" \
-e "s/^health_check_timeout = 20/health_check_timeout = 0/" \
-e "s/^health_check_user = 'nobody'/health_check_user = 'postgres'/" \
-e "s/^health_check_password = ''/health_check_password = 'LUen9N3t8X3sS4LnKEwU'/" \
/usr/local/etc/pgpool.conf

#Failover
#SCP failover script 
sed -i.backup \
-e "s+^failover_command = ''+failover_command = '/usr/local/bin/failover.sh %d %P %H myreplicationpassword /etc/postgresql/9.6/main/im_the_master'+" \
/usr/local/etc/pgpool.conf

# Online recovery
sed -i.backup \
-e "s/^recovery_user = 'nobody'/recovery_user = 'postgres'/" \
-e "s/^recovery_password = ''/recovery_password = 'LUen9N3t8X3sS4LnKEwU'/" \
-e "s+^recovery_1st_stage_command = ''+recovery_1st_stage_command = '/usr/local/bin/recovery_1st_stage.sh'+" \
/usr/local/etc/pgpool.conf

# Memory cache (default: shmem, instead of memcached)
sed -i.backup \
-e "s/^memory_cache_enabled = off/memory_cache_enabled = on/" \
/usr/local/etc/pgpool.conf

# Streaming replication check (left as default)
# Supposed to be default, but just in case:
sed -i.backup \
-e "s/^sr_check_user = 'nobody'/sr_check_user = 'postgres'/" \
-e "s/^sr_check_password = ''/sr_check_password = 'LUen9N3t8X3sS4LnKEwU'/" \
/usr/local/etc/pgpool.conf
#echo "sr_check_period = 5" >> /etc/pgpool2/3.6.6/pgpool.conf

# NO WATCHDOG as pgPool resides in every API server

# Configuring pcp.conf
cp /usr/local/etc/pcp.conf.sample /usr/local/etc/pcp.conf
chown pgpool /usr/local/etc/pcp.conf
echo "pgpool:`pg_md5 gnms1779p`" >> /usr/local/etc/pcp.conf


#No se hace porque probablemente supervisor se encargue
#chmod +x /etc/init.d/pgpool2
#update-rc.d pgpool2 defaults
#update-rc.d pgpool2 disable


#POOL_HBA config
sed -i.backup \
-e "s/^enable_pool_hba = off/enable_pool_hba = on/" \
/usr/local/etc/pgpool.conf
cp /usr/local/etc/pool_hba.conf.sample /usr/local/etc/pool_hba.conf
pg_md5 -f /usr/local/etc/pgpool.conf -m -u postgres 'LUen9N3t8X3sS4LnKEwU'

#modificar method
#host    all         all         127.0.0.1/32          trust -> md5
sed -i.backup \
-e "s+^host    all         all         127.0.0.1/32          trust+host    all         all         127.0.0.1/32          md5+" \
/usr/local/etc/pool_hba.conf
#update-rc.d pgpool2 enable

#PARA PROBAR:
#mkdir -p /var/run/pgpool
#pgpool -n -d > /tmp/pgpool.log 2>&1 &
#sudo -u postgres createdb -p 9999 -h localhost dbname --Pedira password = LUen9N3t8X3sS4LnKEwU

#SOFT LINK OF /ETC/PGPOOL2 TO /USR/LOCAL ??