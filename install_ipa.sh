### FREEIPA
### FROM: https://www.freeipa.org/page/Quick_Start_Guide

## Set hostname and domain name
## hostname: freeipa.intra.glyphstock.com in /etc/hosts and /etc/hostname
## domain name: intra.glyphstock.com  
yum install ipa-server -y
## Si falla httpd, hay que añadir en la configuracion del contenedor 'security.privileged: "true"'

ipa-server-install -p gnms1779p! -a gnms1779p! -n intra.glyphstock.com -r INTRA.GLYPHSTOCK.COM --hostname=ipa.intra.glyphstock.com --no-host-dns