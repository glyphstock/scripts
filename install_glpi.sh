apt update && apt upgrade -y
cd /tmp
curl -OL https://dev.mysql.com/get/mysql-apt-config_0.8.3-1_all.deb
dpkg -i mysql-apt-config*
#Escogemos lo que nos interesa
apt update
rm mysql-apt-config*
apt install mysql-server -y
mysql_secure_installation
#hay que introducir la contraseña del usuario root de mysql
apt install php libapache2-mod-php php-mcrypt php-mysql -y
mysql -u root -p -e "create database glpi;"

#descargamos y descomprimimos
wget https://github.com/glpi-project/glpi/releases/download/9.2.1/glpi-9.2.1.tgz
tar -xzvf glpi-9.2.1.tgz -C /var/www/html/
chmod -R 775 /var/www/html/glpi/


###Para solucionar problemas del test de glpi
#extension ldap
apt install php-ldap -y
#extension imap
apt install php-imap -y
#extension apcu
apt install php-apcu -y
#extension xmlrpc
apt install php-xmlrpc -y
#extension mbstring
apt install php7.0-mbstring -y
#extension curl
apt install php-curl -y
#extension gd
apt install php-gd -y
#extension simplexml
apt install php-xml -y

#Corregir warning sobre "Web access to files directory is protected"
#Pulir el sed para no indicar la linea
sed -i '166s/AllowOverride None/AllowOverride All/' /etc/apache2/apache2.conf

systemctl restart apache2

#Una vez instalado, vamos al navegador web con la direccion 'ip/glpi/'
#Indicamos idioma