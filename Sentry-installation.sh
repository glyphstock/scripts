#apt y apt-get funcionan indistintamente, por si acaso usar apt-get que aparece en todos los tutoriales
#Usar la ip de la máquina y la interfaz de la cual proviene, cambiarlas a mano en el script
ip="$(ifconfig | grep -A 1 'eth0' | tail -1 | cut -d ':' -f 2 | cut -d ' ' -f 1)"
iname=$(ip -o link show | sed -rn '/^[0-9]+: en/{s/.: ([^:]*):.*/\1/p}')
email="systems@glyphstock.com"
pass="gnms1779p!"
#Postgresql: https://www.digitalocean.com/community/tutorials/como-instalar-y-utilizar-postgresql-en-ubuntu-16-04-es
apt update && apt upgrade -y
apt install postgresql postgresql-contrib -y

#crear un usuario en postgres y una base de datos asociada
#Importante que el rol sea superuser, si no el comando "sentry upgrade" no completara algunos pasos y no funcionara el servicio
sudo -i -u postgres psql -c "CREATE USER sentry WITH PASSWORD 'sentry';"
sudo -i -u postgres psql -c "ALTER ROLE sentry SUPERUSER;"
#Creamos la base de datos y damos permisos al usuario
sudo -i -u postgres psql -c "CREATE DATABASE dbsentry;"

#Redis: https://www.digitalocean.com/community/tutorials/how-to-install-and-configure-redis-on-ubuntu-16-04
#Comprobar dependencias
apt install build-essential tcl -y
cd /tmp/
#enlace de descarga siempre estable
curl -O http://download.redis.io/redis-stable.tar.gz
tar xzvf redis-stable.tar.gz
cd redis-stable
#Instalamos y testeamos redis
make install
make test

# Configure redis:
# Change "supervised no" to "supervised systemd"
sed -i 's/^\(supervised\s*\).*$/\1systemd/' redis.conf
# Change "dir ./" to "/var/lib/redis"
sed -i 's/^\(dir\s*\).*$/\1\/var\/lib\/redis/' redis.conf
mkdir /etc/redis
touch /etc/redis/redis.conf
cp /tmp/redis-stable/redis.conf /etc/redis/redis.conf
touch /etc/systemd/system/redis.service
# Redis service config:
echo "[Unit]" >> /etc/systemd/system/redis.service
echo "Description=Redis In-Memory Data Store" >> /etc/systemd/system/redis.service
echo "After=network.target" >> /etc/systemd/system/redis.service
echo "[Service]" >> /etc/systemd/system/redis.service
echo "User=redis" >> /etc/systemd/system/redis.service
echo "Group=redis" >> /etc/systemd/system/redis.service
echo "ExecStart=/usr/local/bin/redis-server /etc/redis/redis.conf" >> /etc/systemd/system/redis.service
echo "ExecStop=/usr/local/bin/redis-cli shutdown" >> /etc/systemd/system/redis.service
echo "Restart=always" >> /etc/systemd/system/redis.service
echo "[Install]" >> /etc/systemd/system/redis.service
echo "WantedBy=multi-user.target" >> /etc/systemd/system/redis.service

#Creamos usuario redis para el servicio
adduser --system --group --no-create-home redis
mkdir /var/lib/redis
chown redis:redis /var/lib/redis
chmod 770 /var/lib/redis
# Enable Redis to Start at Boot
systemctl enable redis
systemctl start redis

#Sentry
#Requisitos minimos: https://docs.sentry.io/server/installation/
#Instalacion: https://docs.sentry.io/server/installation/python/
#Se puede installar mediante Docker o Python, en este caso usaremos Python
#Instalamos pip (gestor de paquetes de Python)
apt install python-pip -y
#Podemos comprobar que esta bien instalado con "pip -V"
#Instalamos virtualenv
pip install -U virtualenv
#Creamos el entorno virtual
virtualenv /www/sentry/
#Entramos en el entorno virtual
source /www/sentry/bin/activate
#Instalamos estos 3 paquetes que permiten la instalacion de sentry
apt install python-psycopg2 -y
apt install libpq-dev -y
apt install libjpeg8-dev -y
pip install -U sentry
sentry init /etc/sentry
#el comando anterior crea los ficheros "sentry.conf.py" y "config.yml"
#actualizar /etc/sentry/sentry.conf.py con la info de la base de datos de postgres creada anteriormente, usuario y pwd y SENTRY_WEB_HOST con la ip de la maquina
#puerto por defecto de sentry es 9000 en SENTRY_WEB_PORT
#Sera algo de este estilo
#DATABASES = {
#    'default': {
#        'ENGINE': 'sentry.db.postgres',
#        'NAME': 'dbsentry',
#        'USER': 'sentry',
#        'PASSWORD': 'sentry',
#        'HOST': 'localhost',
#        'PORT': '',
#        'AUTOCOMMIT': True,
#        'ATOMIC_REQUESTS': False,
#    }
#}
sed -i "s/'NAME': 'sentry',/'NAME': 'dbsentry',/" /etc/sentry/sentry.conf.py
sed -i "s/'USER': 'postgres',/'USER': 'sentry',/" /etc/sentry/sentry.conf.py
sed -i "s/'PASSWORD': '',/'PASSWORD': 'sentry',/" /etc/sentry/sentry.conf.py
sed -i "s/'HOST': '',/'HOST': 'localhost',/" /etc/sentry/sentry.conf.py

sed -i "s/SENTRY_WEB_HOST = '0.0.0.0'/SENTRY_WEB_HOST = '$ip'/" /etc/sentry/sentry.conf.py


SENTRY_CONF=/etc/sentry/ sentry upgrade --noinput
#sentry upgrade nos pedira datos de usuario y la posibilidad de ser superuser (presumiblemente si)
#export C_FORCE_ROOT="true"
#poco seguro
#Creamos usuario para crear el servicio
adduser --system --group --no-create-home sentry
#Creamos 3 ficheros para el servicio: sentry-web.service, sentry-worker.service and sentry-cron.service, en la carpeta /etc/systemd/system
#Sentry-web.service
touch /etc/systemd/system/sentry-web.service
echo "[Unit]" >> /etc/systemd/system/sentry-web.service
echo "Description=Sentry Main Service" >> /etc/systemd/system/sentry-web.service
echo "After=network.target" >> /etc/systemd/system/sentry-web.service
echo "Requires=sentry-worker.service" >> /etc/systemd/system/sentry-web.service
echo "Requires=sentry-cron.service" >> /etc/systemd/system/sentry-web.service

echo "[Service]" >> /etc/systemd/system/sentry-web.service
echo "Type=simple" >> /etc/systemd/system/sentry-web.service
echo "User=sentry" >> /etc/systemd/system/sentry-web.service
echo "Group=sentry" >> /etc/systemd/system/sentry-web.service
echo "WorkingDirectory=/www/sentry" >> /etc/systemd/system/sentry-web.service
echo "Environment=SENTRY_CONF=/etc/sentry" >> /etc/systemd/system/sentry-web.service
echo "ExecStart=/www/sentry/bin/sentry run web" >> /etc/systemd/system/sentry-web.service

echo "[Install]" >> /etc/systemd/system/sentry-web.service
echo "WantedBy=multi-user.target" >> /etc/systemd/system/sentry-web.service

#Sentry-worker.service
touch /etc/systemd/system/sentry-worker.service
echo "[Unit]" >> /etc/systemd/system/sentry-worker.service
echo "Description=Sentry Background Worker" >> /etc/systemd/system/sentry-worker.service
echo "After=network.target" >> /etc/systemd/system/sentry-worker.service

echo "[Service]" >> /etc/systemd/system/sentry-worker.service
echo "Type=simple" >> /etc/systemd/system/sentry-worker.service
echo "User=sentry" >> /etc/systemd/system/sentry-worker.service
echo "Group=sentry" >> /etc/systemd/system/sentry-worker.service
echo "WorkingDirectory=/www/sentry" >> /etc/systemd/system/sentry-worker.service
echo "Environment=SENTRY_CONF=/etc/sentry" >> /etc/systemd/system/sentry-worker.service
echo "ExecStart=/www/sentry/bin/sentry run worker" >> /etc/systemd/system/sentry-worker.service

echo "[Install]" >> /etc/systemd/system/sentry-worker.service
echo "WantedBy=multi-user.target" >> /etc/systemd/system/sentry-worker.service

#Sentry-cron.service
touch /etc/systemd/system/sentry-cron.service
echo "[Unit]" >> /etc/systemd/system/sentry-cron.service
echo "Description=Sentry Beat Service" >> /etc/systemd/system/sentry-cron.service
echo "After=network.target" >> /etc/systemd/system/sentry-cron.service

echo "[Service]" >> /etc/systemd/system/sentry-cron.service
echo "Type=simple" >> /etc/systemd/system/sentry-cron.service
echo "User=sentry" >> /etc/systemd/system/sentry-cron.service
echo "Group=sentry" >> /etc/systemd/system/sentry-cron.service
echo "WorkingDirectory=/www/sentry" >> /etc/systemd/system/sentry-cron.service
echo "Environment=SENTRY_CONF=/etc/sentry" >> /etc/systemd/system/sentry-cron.service
echo "ExecStart=/www/sentry/bin/sentry run cron" >> /etc/systemd/system/sentry-cron.service

echo "[Install]" >> /etc/systemd/system/sentry-cron.service
echo "WantedBy=multi-user.target" >> /etc/systemd/system/sentry-cron.service

sudo systemctl enable sentry-web.service
sudo systemctl start sentry-web.service

#Creamos un usuario para el servicio web
SENTRY_CONF=/etc/sentry sentry createuser --email=$email --password=$pass --superuser
#A partir de aqui si en el navegador ponemos ip:9000 nos llevara al login, ponemos las credenciales y accedemos a la setup final de sentry
#Introducir datos finales y podremos acceder al servicio web