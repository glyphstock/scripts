###More info at https://www.digitalocean.com/community/tutorials/how-to-configure-redis-replication-on-ubuntu-16-04
machine_ip=$(ifconfig | sed -En 's/127.0.0.1//;s/.*inet (addr:)?(([0-9]*\.){3}[0-9]*).*/\2/p')
master_ip="94.130.53.169"

apt-add-repository ppa:chris-lea/redis-server -y
apt update && apt install redis-server -y

#Testeamos que funcione
redis-cli ping

sed -i "s/bind 127.0.0.1/bind 127.0.0.1 $machine_ip/" /etc/redis/redis.conf
ufw allow 6379

#Configure slave
#set master ip and master port
sed -i "s/# slaveof <masterip> <masterport>/slaveof $master_ip 6379/" /etc/redis/redis.conf
#set master auth
sed -i "s/# masterauth <master-password>/masterauth x-54173*2e7gnms1779p\!/" /etc/redis/redis.conf
#set slave auth
sed -i "s/# requirepass foobared/requirepass x-37145*2e7gnms9771p\!/" /etc/redis/redis.conf

#apply changes
systemctl restart redis-server.service

#PROMOTING SLAVE TO MASTER
#Once you are authenticated, run:slaveof no one
#If redis service is restarted, replication will resume
#If slave is promoted to master, it is needed to configure maxmemory policy and durability guarantee policy

#If you want to resume the node to the original master, run: slaveof $master_ip $master_port (6379)