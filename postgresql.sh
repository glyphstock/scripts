# Add the other servers to /etc/hosts:
# IPv4
# 127.0.0.1 localhost.localdomain localhost
# 94.130.53.31  pg1
# 94.130.53.30  pg2
# 94.130.53.169 ha1
# 94.130.54.106 ha2
# 138.201.193.35 api1
# 138.201.193.30 api2
# 138.201.193.29 api3

# APT REPO UPDATE
apt-get install ntp -y
sh -c 'echo "deb http://apt.postgresql.org/pub/repos/apt/ xenial-pgdg main" > /etc/apt/sources.list.d/pgdg.list'
apt-get install wget ca-certificates
wget --quiet -O - https://www.postgresql.org/media/keys/ACCC4CF8.asc | apt-key add -
apt-get update
apt-get upgrade -y
apt-get install postgresql postgresql-contrib -y
# pgpoolII --> postgresql-[postgresql_version]-pgpool2
apt install postgresql-9.6-pgpool2 -y

# sudo -u postgres /usr/lib/postgresql/9.6/bin/pg_ctl -D /var/lib/postgresql/9.6/main -l logfile start

#Change 'postgresql' user's password
sudo -u postgres psql
ALTER USER postgres WITH PASSWORD 'LUen9N3t8X3sS4LnKEwU';
\q

sudo -u postgres psql
CREATE ROLE replication WITH REPLICATION PASSWORD 'JUHNJA8QxAbxXxmxd4kP' LOGIN;
\q

touch /var/lib/postgresql/.pgpass
echo "*:*:*:replication:JUHNJA8QxAbxXxmxd4kP" >> /var/lib/postgresql/.pgpass
chown postgres:postgres /var/lib/postgresql/.pgpass
chmod 0600 /var/lib/postgresql/.pgpass

sed -i \
-e "s/^#listen_addresses = 'localhost'/listen_addresses = '*'/" \
-e "s/^port = 5432/port = 5433/" \
/etc/postgresql/9.6/main/postgresql.conf
#Port remains 5432

echo "host  replication     replication     94.130.53.31/18          md5" >> /etc/postgresql/9.6/main/pg_hba.conf
echo "host  replication     replication     94.130.53.30/18          md5" >> /etc/postgresql/9.6/main/pg_hba.conf

##### ONLY ON PRIMARY SERVER: ################
sed -i.backup \
-e "s/^#wal_level = minimal/wal_level = hot_standby/" \
-e "s/^#max_replication_slots = 0/max_replication_slots = 3/" \
-e "s/^#max_wal_senders = 0/max_wal_senders = 3/" \
/etc/postgresql/9.6/main/postgresql.conf

sudo -u postgres psql
SELECT * FROM pg_create_physical_replication_slot('pg2');
\q

service postgresql restart
#############################################

##### ONLY ON STANDBY SERVERS: ################
service postgresql stop
sudo -i -u postgres
cd /var/lib/postgresql/9.6
rm -rf main
pg_basebackup -v -D main -R -P -h pg1 -p 5433 -U replication
logout

sed -i.backup \
-e "s/^#hot_standby = off/hot_standby = on/" \
-e "s/^#hot_standby_feedback = off/hot_standby_feedback = on/" \
/etc/postgresql/9.6/main/postgresql.conf

echo "trigger_file = '/etc/postgresql/9.6/main/im_the_master'" >> /var/lib/postgresql/9.6/main/recovery.conf
echo "primary_slot_name = 'pg2'" >> /var/lib/postgresql/9.6/main/recovery.conf
service postgresql start
#############################################


#############################################
### REPLICATION WITH SCRIPTS
#############################################

# Enable passwordless SSH for postgres user for every machines!!
# Remeber to trust fingerprint

# Replication templates:
# pg_hba.conf is already the same on all PG servers
chown postgres:postgres /etc/postgresql/9.5/main/pg_hba.conf
sudo -i -u postgres mkdir /etc/postgresql/9.6/main/repltemplates/

# ONLY ON PG1 (From primary to standby):
sudo -i -u postgres scp /etc/postgresql/9.6/main/postgresql.conf postgres@pg2:/etc/postgresql/9.6/main/repltemplates/postgresql.conf.primary
sudo -i -u postgres cp /etc/postgresql/9.6/main/postgresql.conf /etc/postgresql/9.6/main/repltemplates/postgresql.conf.primary
sudo -i -u postgres touch /etc/postgresql/9.6/main/im_the_master
# ONLY ON PG2 (From stanby to primary):
sudo -i -u postgres scp /etc/postgresql/9.6/main/postgresql.conf postgres@pg1:/etc/postgresql/9.6/main/repltemplates/postgresql.conf.standby
sudo -i -u postgres cp /etc/postgresql/9.6/main/postgresql.conf /etc/postgresql/9.6/main/repltemplates/postgresql.conf.standby
sudo -i -u postgres touch /etc/postgresql/9.6/main/im_slave
# ON BOTH AGAIN:
chown postgres:postgres -R /etc/postgresql/9.6/main/repltemplates

# Replication scripts:
mkdir -p /etc/postgresql/9.6/main/replscripts
# COPY SCRIPTS via SCP
chown postgres:postgres -R /etc/postgresql/9.6/main/replscripts
chmod 0744 -R /etc/postgresql/9.6/main/replscripts



#####################################################
### INSTALL pgPool (at API servers):
#####################################################
apt-get install ntp -y
sh -c 'echo "deb http://apt.postgresql.org/pub/repos/apt/ xenial-pgdg main" > /etc/apt/sources.list.d/pgdg.list'
apt-get install wget ca-certificates
wget --quiet -O - https://www.postgresql.org/media/keys/ACCC4CF8.asc | apt-key add -
apt-get update
apt-get upgrade -y
apt install postgresql-9.6
apt-get install postgresql-server-dev-all
apt-get install postgresql-common

apt-get update && apt-get upgrade -y
apt-get install libpq-dev make -y
apt-get install build-essential -y
wget http://www.pgpool.net/download.php?f=pgpool-II-3.6.6.tar.gz -O pgpool-II-3.6.6.tar.gz
tar -xzf pgpool-II-3.6.6.tar.gz
rm pgpool-II-3.6.6.tar.gz
cd pgpool-II-3.6.6

# INSTALL PGPOOL (under /usr/local)
./configure 
#./configure --prefix=/usr/share/pgpool2/3.6.6
make
make install

# PGPOOL-RECOVERY
cd ~/pgpool-II-3.6.6/src/sql/pgpool-recovery
make
make install

sudo -u postgres psql template1
CREATE EXTENSION pgpool_recovery;
\q

echo "pgpool.pg_ctl = '/usr/lib/postgresql/9.6/bin/pg_ctl'" >> /etc/postgresql/9.6/main/postgresql.conf
sudo -i -u postgres /usr/lib/postgresql/9.6/bin/pg_ctl -D /data start

sudo -i -u postgres /usr/lib/postgresql/9.6/bin/pg_ctl -D /var/lib/postgresql/9.6/main restart

# PGPOOL-REGCLASS (Skipped as we are using a later version than PostgreSQL 9.4)
# cd ~/pgpool-II-3.6.6/src/sql/pgpool-recovery
#make
#make install
#sudo -u postgres psql template1
#CREATE EXTENSION pgpool_regclass;
#\q

# INSERT_LOCK TABLE (skipped as we are noy using replication)
#cd pgpool-II-3.6.6/sql
#psql -f insert_lock.sql template1

# CREATE PGPOOLII service
# /etc/default/pgpool2
# CREATE PGPOOLII init.d
# /etc/default/pgpool2
# BOTH VIA SCP
# scp -r etc/default/ api2:/etc/
#scp -r etc/init.d api2:/etc/


# Setting up pgpool.conf (master-slave streaming replication)
cp /usr/local/etc/pgpool.conf.sample-stream /usr/local/etc/pgpool.conf
chown pgpool /usr/local/etc/pgpool.conf

# General settings
#-e "s/^listen_addresses = 'localhost'/listen_addresses = '*'/" \
sed -i.backup \
-e "s+^socket_dir = '/tmp'+socket_dir = '/var/run/postgresql'+" \
-e "s+^pcp_socket_dir = '/tmp'+pcp_socket_dir = '/var/run/postgresql'+" \
/usr/local/etc/pgpool.conf

# Backend settings
sed -i.backup \
-e "s/^backend_hostname0 = 'host1'/backend_hostname0 = 'pg1'/" \
-e "s/^backend_port0 = 5433/backend_port0 = 5433/" \
-e "s+^backend_data_directory0 = '/data'+backend_data_directory0 = '/var/lib/postgresql/9.6/main'+" \
/usr/local/etc/pgpool.conf

sed -i.backup \
-e "s/^#backend_hostname1 = 'host2'/backend_hostname1 = 'pg2'/" \
-e "s/^#backend_port1 = 5433/backend_port1 = 5433/" \
-e "s/^#backend_weight1 = 1/backend_weight1 = 1/" \
-e "s+^#backend_data_directory1 = '/data1'+backend_data_directory1 = '/var/lib/postgresql/9.6/main'+" \
-e "s/^#backend_flag1 = 'ALLOW_TO_FAILOVER'/backend_flag1 = 'ALLOW_TO_FAILOVER'/" \
/usr/local/etc/pgpool.conf

# Pooling settings
# Logging

# Health check
sed -i.backup \
-e "s/^health_check_period = 0/health_check_period = 5/" \
-e "s/^health_check_timeout = 20/health_check_timeout = 0/" \
-e "s/^health_check_user = 'nobody'/health_check_user = 'postgres'/" \
-e "s/^health_check_password = ''/health_check_password = 'LUen9N3t8X3sS4LnKEwU'/" \
/usr/local/etc/pgpool.conf

#Failover
#SCP failover script 
sed -i.backup \
-e "s+^failover_command = ''+failover_command = '/usr/local/bin/failover.sh %d %P %H myreplicationpassword /etc/postgresql/9.6/main/im_the_master'+" \
/usr/local/etc/pgpool.conf

# Online recovery
sed -i.backup \
-e "s/^recovery_user = 'nobody'/recovery_user = 'postgres'/" \
-e "s/^recovery_password = ''/recovery_password = 'LUen9N3t8X3sS4LnKEwU'/" \
-e "s+^recovery_1st_stage_command = ''+recovery_1st_stage_command = '/usr/local/bin/recovery_1st_stage.sh'+" \
/usr/local/etc/pgpool.conf

# Memory cache (default: shmem, instead of memcached)
sed -i.backup \
-e "s/^memory_cache_enabled = off/memory_cache_enabled = on/" \
/usr/local/etc/pgpool.conf

# Streaming replication check (left as default)
# Supposed to be default, but just in case:
sed -i.backup \
-e "s/^sr_check_user = 'nobody'/sr_check_user = 'postgres'/" \
-e "s/^sr_check_password = ''/sr_check_password = 'LUen9N3t8X3sS4LnKEwU'/" \
/usr/local/etc/pgpool.conf
#echo "sr_check_period = 5" >> /etc/pgpool2/3.6.6/pgpool.conf

# NO WATCHDOG as pgPool resides in every API server

# PGPOOL user
useradd pgpool

# Configuring pcp.conf
cp /usr/local/etc/pcp.conf /usr/local/etc/pcp.conf
chown pgpool /usr/local/etc/pcp.conf
echo "pgpool:`pg_md5 gnms1779p`" >> /usr/local/etc/pcp.conf


chmod +x /etc/init.d/pgpool2
update-rc.d pgpool2 defaults
update-rc.d pgpool2 disable


#POOL_HBA config
sed -i.backup \
-e "s/^enable_pool_hba = off/enable_pool_hba = on/" \
/usr/local/etc/pgpool.conf
cp /usr/local/etc/pool_hba.conf.sample /usr/local/etc/pool_hba.conf
echo "host all all 138.201.193.0/18 md5" >> /usr/local/etc/pool_hba.conf
pg_md5 -f /usr/local/etc/pgpool.conf -m -u postgres 'LUen9N3t8X3sS4LnKEwU'
update-rc.d pgpool2 enable
service pgpool2 start

#SOFT LINK OF /ETC/PGPOOL2 TO /USR/LOCAL ??

#####################################################
### INSTALL pgPool (OLD)
#####################################################
# Section: 4.1.1.1. Extensions and SQL Scripts gos here
# Change SQL script files with the appropiate module paths (only on master?)
#sed -i 's+MODULE_PATHNAME+/usr/lib/postgresql/9.6/lib/pgpool-recovery+g' /etc/postgresql/9.6/main/sql/pgpool-recovery.sql
#sed -i 's+MODULE_PATHNAME+/usr/lib/postgresql/9.6/lib/pgpool-regclass+g' /etc/postgresql/9.6/main/sql/pgpool-regclass.sql
#sed -i 's+MODULE_PATHNAME+/usr/lib/postgresql/9.6/lib/pgpool_adm+g' /etc/postgresql/9.6/main/sql/pgpool_adm.sql
#sed -i 's+$libdir/pgpool-recovery+/usr/lib/postgresql/9.6/lib/pgpool-recovery+g' /etc/postgresql/9.6/main/sql/pgpool-recovery.sql
#chown postgres:postgres -R /etc/postgresql/9.6/main/sql



# Failover scripts, etc
# SCP them into the pgpoolserver (first goes into etc.. the other into var...)
chown postgres:postgres /etc/pgpool2/3.6.6/failover.sh
chmod 0700 /etc/pgpool2/3.6.6/failover.sh
chown postgres:postgres /var/lib/postgresql/9.6/main/recovery_1st_stage.sh
chmod 0700 /var/lib/postgresql/9.6/main/recovery_1st_stage.sh
chown postgres:postgres /var/lib/postgresql/9.6/main/pgpool_remote_start
chmod 0700 /var/lib/postgresql/9.6/main/pgpool_remote_start


cp /etc/pgpool2/3.6.6/pcp.conf.sample /etc/pgpool2/3.6.6/pcp.conf
# username:[md5 ancypted password (gnms1779p!)]
echo "postgres:aa28e79a779c1459c8d8be1dda4544a7" >> /etc/pgpool2/3.6.6/pcp.conf


cp /etc/pgpool2/3.6.6/pgpool.conf.sample-stream /etc/pgpool2/3.6.6/pgpool.conf
echo "### CUSTOM SETTINGS" >> /etc/pgpool2/3.6.6/pgpool.conf

echo "pid_file_name = '/var/run/postgresql/pgpool.pid'" >> /etc/pgpool2/3.6.6/pgpool.conf

# IP and ARPING
#Add to sudoers file:
#postgres ALL=(root) NOPASSWD: /bin/ip
#www-data ALL=(root) NOPASSWD: /bin/ip
#postgres ALL=(root) NOPASSWD: /usr/bin/arping
#www-data ALL=(root) NOPASSWD: /usr/bin/arping

#Files:
#ip_w into /bin
#arping_w into /usr/bin

chmod 0755 /bin/ip_w
chmod 0755 /usr/bin/arping_w

echo "if_cmd_path = '/bin'" >> /etc/pgpool2/3.6.6/pgpool.conf
echo "if_up_cmd = 'ip_w addr add $_IP_$/18 dev eth0 label eth0:0'" >> /etc/pgpool2/3.6.6/pgpool.conf
echo "if_down_cmd = 'ip_w addr del $_IP_$/18 dev eth0'" >> /etc/pgpool2/3.6.6/pgpool.conf
echo "arping_path = '/usr/bin'" >> /etc/pgpool2/3.6.6/pgpool.conf
echo "arping_cmd = 'arping_w -U $_IP_$ -w 1'" >> /etc/pgpool2/3.6.6/pgpool.conf

echo "enable_pool_hba = on" >> /etc/pgpool2/3.6.6/pgpool.conf
echo 'pool_passwd = "pool_passwd"' >> /etc/pgpool2/3.6.6/pgpool.conf


cp /etc/pgpool2/3.6.6/pool_hba.conf.sample /etc/pgpool2/3.6.6/pool_hba.conf

echo "host all all 138.201.192.0/18 md5" >> /etc/pgpool2/3.6.6/pool_hba.conf
pg_md5 -f /etc/pgpool2/3.6.6/pgpool.conf -m -u postgres 'gnms1779p!'
update-rc.d pgpool2 enable
service pgpool2 start







sudo -u postgres psql -c "CREATE DATABASE glyphstock"
sudo -u postgres psql -c "CREATE USER glyphadmin WITH PASSWORD 'glyphadmin'"
sudo -u postgres psql -c "ALTER ROLE glyphadmin SET client_encoding TO 'utf8'"
sudo -u postgres psql -c "ALTER ROLE glyphadmin SET default_transaction_isolation TO 'read committed'"
sudo -u postgres psql -c "ALTER ROLE glyphadmin SET timezone TO 'UTC';"
# Add admin rights to user "glyphadmin" (must be done before
#configuring external acces, otherwise a UNIX socket error arises):
sudo -u postgres psql -c "GRANT ALL PRIVILEGES ON DATABASE glyphstock TO glyphadmin;"

# Change "listen_adresses = '*'":
# /etc/postgresql/9.6/main/postgresql.conf
# Allow access (append 'host  all  all 0.0.0.0/0 md5'):
echo "host  all  all 0.0.0.0/0 md5" >> /etc/postgresql/9.6/main/pg_hba.conf
/etc/init.d/postgresql restart

# SSL CERTIFICATE NEEDED??
#apt-get install software-properties-common
#add-apt-repository ppa:certbot/certbot
#apt-get update
#apt-get install certbot
#certbot -d postgresql.intra.glyphstock.com --manual --preferred-challenges dns certonly
