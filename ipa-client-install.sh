#Installation for Ubuntu 16.04 and Ubuntu 17.04

#It is good to run this script as su
#change hostname to $HOSTNAME.intra.glyphstock.com in /etc/hostname and /etc/hosts and reboot
apt update
#check changes with command 'dig'
#dig +short $HOSTNAME.intra.glyphstock.com -> returns ipv4 address
#dig +short -x ipv4 address -> returns $HOSTNAME.intra.glyphstock.com
apt install freeipa-client -y
#Kerberos realm: INTRA.GLYPHSTOCK.COM
#Kerberos server: INTRA.GLYPHSTOCK.COM
#administrative server: ipa.intra.glyphstock.com

ipa-client-install --mkhomedir
#Domain name of IPA server: ipa.intra.glyphstock.com
#IPA server name: ipa.intra.glyphstock.com
#Proceed with fixed values and no DNS discovery? [no]: yes
#Continue to configure the system with these values? [no]: yes
#User authorized to enroll computers: admin
#password for admin: gnms1779p!

##PAM CONFIGURATION FILES FOR UBUNTU 16.04 LTS

## <- en el archivo se han de quitar, son lineas de código

##common-account
#
# /etc/pam.d/common-account - authorization settings common to all services
#
# This file is included from other service-specific PAM config files,
# and should contain a list of the authorization modules that define
# the central access policy for use on the system.  The default is to
# only deny service to users whose accounts are expired in /etc/shadow.
#
# As of pam 1.0.1-6, this file is managed by pam-auth-update by default.
# To take advantage of this, it is recommended that you configure any
# local modules either before or after the default block, and use
# pam-auth-update to manage selection of other modules.  See
# pam-auth-update(8) for details.
#

# here are the per-package modules (the "Primary" block)
##account	[success=1 new_authtok_reqd=done default=ignore]	pam_unix.so 
# here's the fallback if no module succeeds
##account	requisite			pam_deny.so
# prime the stack with a positive return value if there isn't one already;
# this avoids us returning an error just because nothing sets a success code
# since the modules above will each just jump around
##account	required			pam_permit.so
# and here are more per-package modules (the "Additional" block)
##account	sufficient			pam_localuser.so 
##account	[default=bad success=ok user_unknown=ignore]	pam_sss.so 
# end of pam-auth-update config

##common-session
#
# /etc/pam.d/common-session - session-related modules common to all services
#
# This file is included from other service-specific PAM config files,
# and should contain a list of modules that define tasks to be performed
# at the start and end of sessions of *any* kind (both interactive and
# non-interactive).
#
# As of pam 1.0.1-6, this file is managed by pam-auth-update by default.
# To take advantage of this, it is recommended that you configure any
# local modules either before or after the default block, and use
# pam-auth-update to manage selection of other modules.  See
# pam-auth-update(8) for details.

# here are the per-package modules (the "Primary" block)
##session	[default=1]			pam_permit.so
# here's the fallback if no module succeeds
##session	requisite			pam_deny.so
# prime the stack with a positive return value if there isn't one already;
# this avoids us returning an error just because nothing sets a success code
# since the modules above will each just jump around
##session	required			pam_permit.so
# The pam_umask module will set the umask according to the system default in
# /etc/login.defs and user settings, solving the problem of different
# umask settings with different shells, display managers, remote sessions etc.
# See "man pam_umask".
##session optional			pam_umask.so
# and here are more per-package modules (the "Additional" block)
##session	required pam_mkhomedir.so umask=0022 skel=/etc/skel
##session	required	pam_unix.so 
##session	optional			pam_sss.so 
##session	optional	pam_systemd.so 
##session	required                        pam_mkhomedir.so umask=0022 skel=/etc/skel
##session	optional	pam_cgfs.so -c freezer,memory,name=systemd
# end of pam-auth-update config

##common-auth
#
# /etc/pam.d/common-auth - authentication settings common to all services
#
# This file is included from other service-specific PAM config files,
# and should contain a list of the authentication modules that define
# the central authentication scheme for use on the system
# (e.g., /etc/shadow, LDAP, Kerberos, etc.).  The default is to use the
# traditional Unix authentication mechanisms.
#
# As of pam 1.0.1-6, this file is managed by pam-auth-update by default.
# To take advantage of this, it is recommended that you configure any
# local modules either before or after the default block, and use
# pam-auth-update to manage selection of other modules.  See
# pam-auth-update(8) for details.

# here are the per-package modules (the "Primary" block)
##auth	[success=2 default=ignore]	pam_unix.so nullok_secure
##auth	[success=1 default=ignore]	pam_sss.so use_first_pass
# here's the fallback if no module succeeds
##auth	requisite			pam_deny.so
# prime the stack with a positive return value if there isn't one already;
# this avoids us returning an error just because nothing sets a success code
# since the modules above will each just jump around
##auth	required			pam_permit.so
# and here are more per-package modules (the "Additional" block)
# end of pam-auth-update config

##common-password
#
# /etc/pam.d/common-password - password-related modules common to all services
#
# This file is included from other service-specific PAM config files,
# and should contain a list of modules that define the services to be
# used to change user passwords.  The default is pam_unix.

# Explanation of pam_unix options:
#
# The "sha512" option enables salted SHA512 passwords.  Without this option,
# the default is Unix crypt.  Prior releases used the option "md5".
#
# The "obscure" option replaces the old `OBSCURE_CHECKS_ENAB' option in
# login.defs.
#
# See the pam_unix manpage for other options.

# As of pam 1.0.1-6, this file is managed by pam-auth-update by default.
# To take advantage of this, it is recommended that you configure any
# local modules either before or after the default block, and use
# pam-auth-update to manage selection of other modules.  See
# pam-auth-update(8) for details.

# here are the per-package modules (the "Primary" block)
##password	requisite			pam_pwquality.so retry=3
##password	[success=2 default=ignore]	pam_unix.so obscure use_authtok try_first_pass sha512
##password	sufficient			pam_sss.so use_authtok
# here's the fallback if no module succeeds
##password	requisite			pam_deny.so
# prime the stack with a positive return value if there isn't one already;
# this avoids us returning an error just because nothing sets a success code
# since the modules above will each just jump around
##password	required			pam_permit.so
# and here are more per-package modules (the "Additional" block)
##password	optional	pam_gnome_keyring.so 
# end of pam-auth-update config

##/usr/share/lightdm/lightdm.conf.d/50-ubuntu.conf
#
##[Seat:*]
##user-session=ubuntu
##greeter-show-manual-login=true

#----------------------------------------------------#

##UBUNTU 17.04
#Con realizar un login por ssh parece ser que se crea el usuario y por consiguiente se puede realizar un login mediante GUI. Aun así
#puede haber un bucle en el login.

#----------------------------------------------------#

#Installation for Windows (only works in pro edition)

#Añadir la máquina manualmente en la página web del servidor de FreeIpa (indicar Ip y poner nombre completo)
#Obtener una keytab para el ordenador con Windows
#Ejecutar los dos siguientes comandos:
#kinit admin
#ipa-getkeytab -s ipa.example.com -p host/windows.example.com -e arcfour-hmac -k krb5.keytab.windows -P
#En la máquina de windows ejecutar los siguientes comandos (la terminal hay que abrirla en modo administrador)
#ksetup /setdomain [REALM]
#ksetup /addkdc [REALM] [KDC]
#ksetup /addkpasswd [REALM] [KDC]
#ksetup /setcomputerpassword [PASSWORD]
#ksetup /mapuser * *
#Pulsar tecla Windows + r y ejecutar gpedit.msc
#Desde la ventana que se abrirá ir a Windows Settings > Security Settings > Local Policies > Security Options > Network Security
#Ir a la opción Configurar tipos de encriptación permitidos para Kerberos
#Seleccionamos todas menos las dos primeras opciones que ponen DES
#Reiniciar la máquina
#Pulsar tecla Windows + r y ejecutar mmc
#Desde la ventana que se abrirá seleccionar File > Add/Remove Snap-in
#En la siguiente ventana, selecciona Local Users and Groups, haz click en “Add >” seguido de Finalizar y después OK.
#A partir de aqui puedes crear usuarios locales en Windows. No hace falta poner contraseña. Además, los nombres de usuarios han de coincidir con los nombres de los usuarios de FreeIPA.
#Por defecto los usuarios recien creados no pueden conectare por escritorio remoto. Para dar pemisos hay que ir a la carpeta "Grupos" en la ventana que tenemos anteriormente 
#y añadir los usuarios a la opcion de usuarios de Escritorio Remoto.

#Reference:
#https://www.rootusers.com/how-to-login-to-windows-with-a-freeipa-account/

#----------------------------------------------------#

#Installation for MAC OS X (## son cosas que no se han de descomentar, son indicaciones)

##DNS Setup
#Either,

#Go to System Preferences>Network
#Select top priority network and click Advanced…
#Select DNS
#Add your IPA server’s IP Address
#Click OK
#Click Apply
#Or, configure your DHCP service to set your IPA server as primary DNS.

##SSL Setup
#Download the ca.crt from the IPA server
#Open terminal
#cd ~/desktop
#curl -OL http://yourserver.yourdomain.com/ipa/config/ca.crt
#Doubleclick the ‘ca.crt’ file
#Add to the System keychain
#Locate certificate within Keychain Access
#Doubleclick the certificate
#Expand Trust
#Change System Default to Always Trust
#Exit Keychain Access and authenticate to apply changes
#Move the ca.crt file to /etc/ipa

##Kerberos Setup
#Edit/create the file /etc/krb5.conf as shown below:

#[domain_realm]
#    .yourdomain.com = YOURDOMAIN.COM
#    yourdomain.com = YOURDOMAIN.COM

#[libdefaults]
#    default_realm = YOURDOMAIN.COM
#    allow_weak_crypto = yes	
#    dns_lookup_realm = true
#    dns_lookup_kdc = true
#    rdns = false
#    ticket_lifetime = 24h
#    forwardable = yes 
#    renewable = true
 
#[realms]
#    YOURDOMAIN.COM = {
#        kdc = tcp/ipa-server.yourdomain.com
#        admin_server = tcp/ipa-server.yourdomain.com
#        pkinit_anchors = FILE:/etc/ipa/ca.crt
#    }

#Edit /etc/pam.d/authorization as shown below:
## authorization: auth account
#auth          optional       pam_krb5.so use_first_pass use_kcminit default_principal
#auth          sufficient     pam_krb5.so use_first_pass default_principal
#auth          required       pam_opendirectory.so use_first_pass nullok
#account    required       pam_opendirectory.so

#Edit screensaver and passwd as shown below
#cat > /etc/pam.d/screensaver << 'EOF'
#auth       optional       pam_krb5.so use_first_pass use_kcminit
#auth       optional       pam_krb5.so use_first_pass use_kcminit default_principal
#auth       sufficient     pam_krb5.so use_first_pass default_principal
#auth       required       pam_opendirectory.so use_first_pass nullok
#account    required       pam_opendirectory.so
#account    sufficient     pam_self.so
#account    required       pam_group.so no_warn group=admin,wheel fail_safe
#account    required       pam_group.so no_warn deny group=admin,wheel ruser fail_safe
#EOF

#cat > /etc/pam.d/passwd << 'EOF'
#password   sufficient     pam_krb5.so
#auth       required       pam_permit.so
#account    required       pam_opendirectory.so
#password   required       pam_opendirectory.so
#session    required       pam_permit.so 
#EOF

#Verify by running “kinit username”

##IPA Enrollment
##Name workstation
#Open terminal
#sudo scutil --set HostName workstation.yourdomain.com

##Add via freeIPA web console
#Open IPA web console (https://yourserver.yourdomain.com)
#Sign on as a Directory Manager
#Go to Identity > Hosts
#Click the + Add button
#Enter the workstation’s hostname (e.g., Book001)
#Add current primary IP address (terminal > # ifconfig)
#Click the Add and Edit button.
#Add the workstation’s MAC addresses

##Generate keytab on IPA server
#su root
#kinit admin
#ipa-getkeytab -s yourserver.yourdomain.com -p host/workstation.yourdomain.com -k ~/workstation.keytab
#To test that the keytab successfully retrieved and stored in ~/workstation.keytab, run ipa host-show workstation
#The previous should return,
#Host name: workstation.yourdomain.com   Principal name: host/workstation.yourdomain.com@YOURDOMAIN.COM
#  	MAC address: 00:00:00:AA:1B:14
#  	Password: False
#  	Keytab: True
#  	Managed by: workstation.yourdomain.com

##Retrieve keytab from server
#From the workstation run sftp admin@yourserver.yourdomain.com
#sftp> get workstation.keytab /etc/krb5.keytab
#sftp> exit
#chown root:wheel /etc/krb5.keytab
#chmod 0600 /etc/krb5.keytab
#Verify on freeIPA web GUI that Kerberos key is present (Identity > Host > workstation)

##Directory Utility Setup
#On workstation, go to System Preferences > Users & Groups > Login Options
#Set the following:
#Automatic login: off
#Display login window as: Name and password
#Show fast user switching menu as Full Name
#Click Join… beside Network Account Server
#Enter “yourserver.yourdomain.com”
#Click Continue
#Verify Allow network users to log in at login window is selected
#Click on Options... beside the previous setting
#Verify All network users is selected
#Next to Network Account Server, click Edit…
#Click Open Directory Utility
#Edit LDAPv3
#Select yourserver.yourdomain.com and choose Edit…
#Set the following:
#Open/close times out in 5 seconds
#Query times out in 5 seconds
#Connection idles out in 1 minute
#Encrypt using SSL (selected)

##Mappings
#From the edit window opened in previous step (Connection), click Search & Mappings
#Add record type Groups and map to ‘ipausergroup’
#Add PrimaryGroupID attribute to Groups and map to ‘gidNumber’
#Add RecordName attribute to Groups and map to ‘cn’
#Add record type Users and map to the following:
#inetOrgPerson
#posixAccount
#shadowAccount
#apple-user
#Within the record type Users add the following with the mappings shown on the right:
#Attribute	Mapping
#AuthenticationAuthority	uid
#GeneratedUID	GeneratedUID
#HomeDirectory	#/Users/$uid$
#NFSHomeDirectory	#/Users/$uid$
#PrimaryGroupID	gidNumber
#RealName	cn
#RecordName	uid
#UniqueID	uidNumber
#UserShell	loginShell
#Verify the search base for both Record Types is “dc=yourdomain,dc=com”
#Verify all subtrees is selected for both Record Types
#Click OK button to save and return to server list
#Click OK again
#Click on Search Policy
#Verify “/LDAPV3/yourserver.yourdomain.com” is listed beneath “/Local/Default”
#Close open windows
#Open terminal and run test “dscacheutil -q user -a name yourusername”

#Reference:
#https://www.freeipa.org/page/HowTo/Setup_FreeIPA_Services_for_Mac_OS_X_10.12#IPA_Enrollment