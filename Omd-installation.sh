#Hay que guardar en la variable 'ip' la direccion de la máquina que va a contener el servicio omd
ip="10.100.10.102"
#https://www.digitalocean.com/community/tutorials/how-to-use-open-monitoring-distribution-with-check_mk-on-ubuntu-14-04#setting-up-the-omd-instance-on-an-ubuntu-dropletapt 
apt update && apt upgrade -y
wget https://mathias-kettner.de/support/1.2.8p15/check-mk-raw-1.2.8p15_0.xenial_amd64.deb
dpkg -i check-mk-raw-1.2.8p15_0.xenial_amd64.deb
apt -f install -y
omd create monitoring
omd start monitoring
#acceder al navegador con http://ip_address/monitoring/
#las credenciales por defecto son 'omdadmin' con contraseña 'omd' 

#Instalación cliente
apt install check-mk-agent
apt install xinetd #puede que ya este instalado
#habilitar puerto 6556
#editar /etc/xinetd.d/check_mk
#descomentar line #from_only=... y poner la ip de la maquina que contiene el servidor OMD
sed -i "s/\t#only_from/\tonly_from/" /etc/xinetd.d/check_mk #sirve
sed -i "s/127.0.0.1 10.0.20.1 10.0.20.2/$ip/" /etc/xinetd.d/check_mk

#si xinetd no estaba installado
service xinetd start
service xinetd enable

#si lo estaba
service xinetd restart

#Setup mail
apt install mailutils
#Edit ssmtp conf at /etc/ssmtp/ssmtp.conf (## <- uncomment)
#Should be like this:
#-------------------------------------------------------------------------------------#
# Config file for sSMTP sendmail
#
# The person who gets all mail for userids < 1000
# Make this empty to disable rewriting.
##root=systems@glyphstock.com

# The place where the mail goes. The actual machine name is required no
# MX records are consulted. Commonly mailhosts are named mail.domain.com
##mailhub=smtp.gmail.com:465

##AuthUser=systems@glyphstock.com
##AuthPass=gnms1779p!
##AuthMethod=LOGIN
##UseTLS=YES
#UseSTARTTLS=YES <--keep commented, it's not a typo
##TLS_CA_File=/etc/ssl/certs/ca-certificates.crt

# Where will the mail seem to come from?
##rewriteDomain=gmail.com

# The full hostname
##hostname=omd.intra.glyphstock.com

# Are users allowed to set their own From: address?
# YES - Allow the user to specify their own From: address
# NO - Use the system generated From: address
##FromLineOverride=YES
#-------------------------------------------------------------------------------------#

#Setup Notifications
#Help at http://blog.unicsolution.com/2014/02/how-to-setup-omd-in-1-hour.html
#More help at https://mathias-kettner.com/cms_notifications.html
