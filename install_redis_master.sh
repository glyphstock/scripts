###More info at https://www.digitalocean.com/community/tutorials/how-to-configure-redis-replication-on-ubuntu-16-04
machine_ip=$(ifconfig | sed -En 's/127.0.0.1//;s/.*inet (addr:)?(([0-9]*\.){3}[0-9]*).*/\2/p')

apt-add-repository ppa:chris-lea/redis-server -y
apt update && apt install redis-server -y

#Testeamos que funcione
redis-cli ping

sed -i "s/bind 127.0.0.1/bind 127.0.0.1 $machine_ip/" /etc/redis/redis.conf
ufw allow 6379

#Configure master
#Autentication to master
sed -i "s/# requirepass foobared/requirepass x-54173*2e7gnms1779p\!/" /etc/redis/redis.conf
#Remove keys when maxmemory is reached
sed -i "s/# maxmemory-policy noeviction/maxmemory-policy volatile-lru/" /etc/redis/redis.conf
#Durability guarantee
sed -i "s/appendonly no/appendonly yes/" /etc/redis/redis.conf
sed -i "s/appendfilename \"appendonly.aof\"/appendfilename \"redis-staging-ao.aof\"/" /etc/redis/redis.conf

#apply changes
systemctl restart redis-server.service