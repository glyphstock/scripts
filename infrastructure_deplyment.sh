lxc launch ubuntu:16.04 haproxy --p br0100

lxc launch ubuntu:16.04 postgresql --p br0100

lxc launch ubuntu:16.04 gunicorn --p br0100

lxc launch ubuntu:16.04 cassandra --p br0100

lxc launch ubuntu:16.04 redis --p br0100

lxc launch ubuntu:16.04 rabbitmq --p br0100
