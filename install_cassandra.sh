machine_ip=$(ifconfig | sed -En 's/127.0.0.1//;s/.*inet (addr:)?(([0-9]*\.){3}[0-9]*).*/\2/p')

umount -a
echo "tmpfs /tmp tmpfs rw,noexec,nosuid,async,noatime,size=5G,nodev 0 0" >> /etc/fstab
mount

### CASSANDRA
### From http://cassandra.apache.org/download/
echo "deb http://www.apache.org/dist/cassandra/debian 311x main" | sudo tee -a /etc/apt/sources.list.d/cassandra.sources.list
curl https://www.apache.org/dist/cassandra/KEYS | sudo apt-key add -
apt update
apt install cassandra -y
service cassandra stop
rm -rf /var/lib/cassandra/data/system/*

### Setting replication
### From https://docs.datastax.com/en/cassandra/latest/cassandra/initialize/initSingleDS.html

## Cluster name
sed -i.backup -e "s/cluster_name: 'Test Cluster'/cluster_name: 'Glyphstock Cluster'/" /etc/cassandra/cassandra.yaml

## Seeds
sed -i.backup -e "s/- seeds: \"127.0.0.1\"/- seeds: \"94.130.35.135,88.99.145.84\"/" /etc/cassandra/cassandra.yaml

## Listen_address
sed -i.backup -e "s/listen_address: localhost/listen_address: $machine_ip/" /etc/cassandra/cassandra.yaml

## Rpc_address (use machine ip)
sed -i.backup -e "s/rpc_address: localhost/rpc_address: 0.0.0.0/" /etc/cassandra/cassandra.yaml

## Endpoint snitch
sed -i.backup -e "s/endpoint_snitch: SimpleSnitch/endpoint_snitch: GossipingPropertyFileSnitch/" /etc/cassandra/cassandra.yaml


## Datacenter and rack name
sed -i.backup -e "s/dc=dc1/dc=DC1/" /etc/cassandra/cassandra-rackdc.properties

sed -i.backup -e "s/rack=rack1/rack=RACK1/" /etc/cassandra/cassandra-rackdc.properties

service cassandra start

### Probar funcionamiento
## Escritura: cassandra-stress write n=1000 -node ipmaquina -schema "replication(strategy=NetworkTopologyStrategy, existing=2)" keyspace="glyph_keyspace"
## Lectura:   cassandra-stress read n=1000 -node ipmaquina -schema "replication(strategy=NetworkTopologyStrategy, existing=2)" keyspace="glyph_keyspace"

##Optimization
#Concurrent writes: 8 x number of cpu cores
sed -i "s/concurrent_writes: 32/concurrent_writes: 64" /etc/cassandra/cassandra.yaml
#commitlog_segment_size_in_mb
sed -i "s/commitlog_segment_size_in_mb: 32/commitlog_segment_size_in_mb: 16" /etc/cassandra/cassandra.yaml